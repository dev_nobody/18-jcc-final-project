@extends('layout.master')
@section('judul')
Halaman Edit Profile
@endsection

@section('content')
    <form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
        @csrf 
        @method('put')

        <div class="form-group">            
            <label>nama user</label>
            <input type="text" value="{{$profile->user->name}}" class="form-control"disabled >
        </div>
        <div>
            <label>Email</label>
            <input type="text" value="{{$profile->user->email}}" class="form-control"disabled >
        </div>
        <div class="form-group">
        <label for="exampleInputEmail1">Umur</label>
        <input type="number" value="{{$profile->umur}}" class="form-control" name="umur">
        </div>

        @error('umur')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror

        <div class="form-group">
            <label for="exampleInputEmail1">alamat</label>
            <textarea name="alamat" class="form-control"  placeholder="ringkasan">{{$profile->alamat}}</textarea>
            </div>
    
            @error('alamat')
            <div class=”alert alert-danger”>{{ $message }}
            </div>
            @enderror

            <div class="form-group">
                <label for="exampleInputEmail1">bio</label>
                <textarea name="bio" class="form-control"  placeholder="ringkasan">{{$profile->bio}}</textarea>
                </div>
        
                @error('bio')
                <div class=”alert alert-danger”>{{ $message }}
                </div>
                @enderror

                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/" class="btn btn-warning ml-5">Kembali</a>
    </form>
@endsection