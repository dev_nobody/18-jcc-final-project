<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-1">
                <div class="header__logo">
                    <a href="/">
                        <img src="{{('layout/img/logo.png')}}">
                    </a>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="header__nav">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li class="active"><a href="/">Homepage</a></li>
                            <li class="active"><a href="/film">Film</a></li>
                            <li class="active"><a href="/cast">Cast</a></li>
                            <li class="active"><a href="/genre">Genre</a></li>
                            <li class="search-switch"><a href="#"></a>
                                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

                                <!-- The form -->
                                <form class="example" action="action_page.php">
                                <input type="text" placeholder="Search.." name="search">
                                <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>
                            <li class="active"><a href="/profil"><span class="icon_profile"></span></a></li>
                            @auth
                            <li class="nav-item bg-danger">
                                <a class="nav-link" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </li>
                            @endauth
                            @guest
                            <li class="active"><a href="/login" class="bg-info">Login</a></li>
                            <li class="active"><a href="/register">Sign Up</a></li>
                            @endguest
                            
                        </ul>
                    </nav>
                </div>
            </div>
            
        </div>
    <div id="mobile-menu-wrap"></div>    
    </div>
</header>