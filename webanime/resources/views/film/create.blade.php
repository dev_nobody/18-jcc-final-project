@extends('layout.master')

@section('crud')
Film
@endsection

@section('judul')
Halaman Tambah Film
@endsection

@push('style')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
  // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
  </script>
@endpush

@push('script')
  <script src="https://cdn.tiny.cloud/1/7qnnlkuknh3kgb3hgc79492u34cckewpjkvernwalfzwt6xo/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')
 
<form action="/film" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label style="color:red;">Judul Film</label>
      <input type="text" name='judul' class="form-control">
    </div>
    @error('judul')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label style="color:red;">Tahun</label>
        <input type="number" name='tahun' class="form-control">
      </div>
      @error('tahun')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
    <div class="form-group">
        <label style="color:red;">Ringkasan Film</label>
        <textarea type="text" name='ringkasan' class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('ringkasan')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label style="color:red">Genre Film</label><br>
        <select type="text" name='genre_id' class="js-example-basic-single" style="width: 100%">
            <option value="">---Pilih Genre---</option>
            @foreach ($genre as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    @error('genre_id')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <br><br>
    <div class="form-group">
        <label style="color:red;">Poster Film</label>
        <input type="file" name='poster' class="form-control">
      </div>
      @error('poster')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
      
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/film" class="btn btn-warning ml-3">Kembali</a>
    
</form>


@endsection
@include('sweetalert::alert')