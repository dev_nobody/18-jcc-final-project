@extends('layout.master')

@section('crud')
    Review Film
@endsection

@section('judul')
Halaman Review Film
@endsection


@section('content')
@auth
    <a href="/film/create" class="btn btn-primary my-2">Input Review Film Baru</a>
@endauth
<div class="row">
    @forelse ($film as $item)
    <div class="col-5 ml-4 my-3">
        <div class="card" style="width: 18rem;">
            <img src="{{asset('filmimage/'. $item->poster)}}" class="card-img-top" alt=" ">
            <div class="card-body">
            <span class="badge badge-info">{{$item->genre->nama}}</span>
            <h3>{{$item->judul}}</h3>
            <p class="card-text">{{Str::limit ($item->ringkasan, 30)}}</p>    
           @auth
            <form action="/film/{{$item->id}}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>   
           @endauth
            
           @guest
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
           @endguest
            </div>
        </div>
    </div>
    <br><br>    
    @empty
    <h4>Data Film belum ada</h4>
    @endforelse
    
</div>
@include('sweetalert::alert')
@endsection
