@extends('layout.master')

@section('crud')
    Edit Film
@endsection

@section('judul')
Halaman Edit Film
@endsection

@section('content')
 
<form action="/film/{{$film->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label style="color:red;">Judul Film</label>
      <input type="text" value="{{$film->judul}}" name='judul' class="form-control">
    </div>
    @error('judul')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label style="color:red;">Tahun</label>
        <input type="number" value="{{$film->tahun}}" name='tahun' class="form-control">
      </div>
      @error('tahun')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
    <div class="form-group">
        <label style="color:red;">Ringkasan Film</label>
        <textarea type="text" name='ringkasan' class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label style="color:red;">Genre Film</label><br>
        <select type="text" name='genre_id' class="form-control">
            <option value="">---Pilih Genre---</option>
            @foreach ($genre as $item)
            @if ($item->id === $film->genre_id)

            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
                
            @endforeach
        </select><br><br>
    </div>
    @error('genre_id')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label style="color:red;">Poster Film</label>
        <input type="file" name='poster' class="form-control">
      </div>
      @error('poster')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/film" class="btn btn-warning ml-3">Kembali</a>
    
</form>

@include('sweetalert::alert')
@endsection