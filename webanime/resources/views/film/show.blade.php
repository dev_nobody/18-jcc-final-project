<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime Details</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('layout/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/elegant-icons.css')}}"type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('partial.nav')
    <!-- Header End -->

    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="/"><i class="fa fa-home"></i> Home</a>
                        <span>Romance</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Anime Section Begin -->
    <section class="anime-details spad">
        <div class="container">
            <div class="anime__details__content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="anime__details__pic set-bg" data-setbg="{{asset('filmimage/'. $film->poster)}}">
                            
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="anime__details__text">
                            <div class="anime__details__title">
                                <h3>{{$film->judul}}</h3>
                                <span></span>
                            </div>
                        
                            <p>{{$film->ringkasan}}</p>
                            <div class="anime__details__widget">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <ul>
                                           
                                            <li><span>Genre:</span> {{$film->genre->nama}}</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                    
                                    </div>
                                </div>
                            </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        <div class="anime__details__review">
                            <div class="section-title">
                                <h5>Reviews</h5>
                                @foreach ($film->kritik as $item)
                                <div class="card">
                                    <div class="card-body">
                                    <h5 style="color:rgb(253, 4, 4);">{{$item->user->name}}</h5>
                                    <p class="card-text">{{$item->isi}}</p>
                                    </div>
                                </div>
                        @endforeach 
                            </div>
                           
                        <div class="anime__details__form">
                            <div class="section-title">
                                <h5>Your Comment</h5>
                            </div>
                            <form action="/kritik" method="post" enctype="multipart/form-data" class="my-3">
                                @csrf
                                <div class="form-group">
                                    <input type="hidden" name="film_id" value="{{$film->id}}" id="">
                                    <textarea name='isi' class="form-control" cols="30" rows="10"></textarea>
                                    <button type="submit" class="btn btn-primary">Submit  </button>
                                    <a href="/film" class="btn btn-warning ml-3">Kembali</a>
                                </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
        <!-- Anime Section End -->

        <!-- Footer Section Begin -->
       @include('partial.footer')
          <!-- Footer Section End -->

          <!-- Search model Begin -->
          <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch"><i class="icon_close"></i></div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search model end -->

        <!-- Js Plugins -->
        <script src="{{asset('layout/js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('layout/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('layout/js/player.js')}}"></script>
        <script src="{{asset('layout/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('layout/js/mixitup.min.js')}}"></script>
        <script src="{{asset('layout/js/jquery.slicknav.js')}}"></script>
        <script src="{{asset('layout/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('layout/js/main.js')}}"></script>
        @include('sweetalert::alert')
    </body>

    </html>