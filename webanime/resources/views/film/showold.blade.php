@extends('layout.master')

@section('crud')
    Detail Film
@endsection

@section('judul')
Halaman Detail Film {{$film->judul}}
@endsection


@section('content')

<img src="{{asset('filmimage/'. $film->poster)}}" alt="">
<br><br><h1 style="color:rgb(252, 8, 8);">{{$film->judul}}</h1><br>
<p class="text-white bg-dark">{{$film->ringkasan}}</p><br>

<h2 style="color:rgb(253, 4, 4);">Kritik</h2>
@foreach ($film->kritik as $item)
          <div class="card">
              <div class="card-body">
              <h5 style="color:rgb(253, 4, 4);">{{$item->user->name}}</h5>
              <p class="card-text">{{$item->isi}}</p>
              </div>
          </div>
@endforeach 


<form action="/kritik" method="post" enctype="multipart/form-data" class="my-3">
    @csrf
    <div class="form-group">
		<input type="hidden" name="film_id" value="{{$film->id}}" id="">
        <textarea name='isi' class="form-control" cols="30" rows="10"></textarea>
    </div>

    @error('isi')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit </button>
    <a href="/film" class="btn btn-warning ml-3">Kembali</a>
    
</form>

@endsection