<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('layout/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/elegant-icons.css')}}"type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
   @include('partial.nav')
    <!-- Header End -->

    <!-- Hero Section Begin -->
    <section class="hero" @yield('content')>
        <div class="container">
            <div class="hero__slider owl-carousel">
                <div class="hero__items set-bg" data-setbg="{{('layout/img/hero/hero-1.jpg')}}">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <div class="label">Adventure</div>
                                <h2>Fate / Stay Night: Unlimited Blade Works</h2>
                                <p>After 30 days of travel across the world...</p>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hero__items set-bg" data-setbg="{{('layout/img/hero/hero-1.jpg')}}">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <div class="label">Adventure</div>
                                <h2>Fate / Stay Night: Unlimited Blade Works</h2>
                                <p>After 30 days of travel across the world...</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hero__items set-bg" data-setbg="{{asset('layout/img/hero/hero-1.jpg')}}">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <div class="label">Adventure</div>
                                <h2>Fate / Stay Night: Unlimited Blade Works</h2>
                                <p>After 30 days of travel across the world...</p>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                   
                   
                    <div class="recent__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Recently Added Shows</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="btn__all">
                                    <a href="/film" class="primary-btn">View All <span class="arrow_right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @forelse ($film as $item)
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="{{asset('filmimage/'. $item->poster)}}">
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li>{{$item->genre->nama}}</li>
                                        </ul>
                                        <h5><a href="/film/{{$item->id}}">{{$item->judul}}</a></h5>
                                       
                                    </div>
                                </div>
                            </div>
                             @empty
                                        <h4>Data Film belum ada</h4>
                                        @endforelse
                        </div>
                    </div>
                </div>
               
        </div>
    </div>
</div>
</div>
</div>
</div>
</section>
<!-- Product Section End -->

<!-- Footer Section Begin -->
@include('partial.footer')
  <!-- Footer Section End -->

  <!-- Search model Begin -->
  <div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch"><i class="icon_close"></i></div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
   
</div>
<!-- Search model end -->

<!-- Js Plugins -->
<script src="{{asset('layout/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('layout/js/bootstrap.min.js')}}"></script>
<script src="{{asset('layout/js/player.js')}}"></script>
<script src="{{asset('layout/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('layout/js/mixitup.min.js')}}"></script>
<script src="{{asset('layout/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('layout/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('layout/js/main.js')}}"></script>


</body>

</html>