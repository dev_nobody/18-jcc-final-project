<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime </title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('layout/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/elegant-icons.css')}}"type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
   @include('partial.nav')
    <!-- Header End -->

    <!-- Hero Section Begin -->
   
      <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="/"><i class="fa fa-home"></i> Home</a>
                        <span>@yield('crud')</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Product Section Begin -->
    <section class="product-page spad">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product__page__content">
                        <div class="product__page__title">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-3">
                                    <div class="section-title">
                                        <h4>@yield('judul')</h4>
                                        <div class="card-body mt-0" >
                                            @yield('content')
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
      
</section>


<!-- Footer Section Begin -->
@include('partial.footer')
<!-- Footer Section End -->

  <!-- Search model Begin -->
  <div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch"><i class="icon_close"></i></div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search model end -->

<!-- Js Plugins -->
<script src="{{asset('layout/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('layout/js/bootstrap.min.js')}}"></script>
<script src="{{asset('layout/js/player.js')}}"></script>
<script src="{{asset('layout/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('layout/js/mixitup.min.js')}}"></script>
<script src="{{asset('layout/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('layout/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('layout/js/main.js')}}"></script>

@yield('section')
@include('sweetalert::alert')

</body>

</html>