@extends('layout.master')
@section('crud')
    Caster
@endsection

@section('judul')
Halaman Menambahkan Data Cast
@endsection

@push('script')
  <script src="https://cdn.tiny.cloud/1/7qnnlkuknh3kgb3hgc79492u34cckewpjkvernwalfzwt6xo/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
        <label for="exampleInputEmail1" style="color:red;">Nama Caster</label>
        <input type="string" class="form-control" name="nama">
        </div>

        @error('nama')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror

        <div class="form-group">
        <label for="exampleInputEmail 1" style="color:red;">Umur</label>
        <input type="integer" class="form-control" name="umur">
        </div>

        @error('umur')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror

        <div class="form-group">
        <label style="color:red;">Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('bio')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/cast" class="btn btn-warning ml-5">Kembali</a>

    </form>     
@endsection