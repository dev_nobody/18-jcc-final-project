@extends('layout.master')
@section('crud')
Caster
@endsection
@section('judul')
Halaman Detail Data Cast
@endsection
@section('content')

<h1 style="color:rgb(255, 255, 255)"> {{$cast->nama}}/{{$cast->umur}} tahun</h1><br>
<h3 style="color:rgb(255, 255, 255)">{{$cast->bio}}</h3><br>
<a href="/cast" class="btn btn-warning ml">Kembali</a>
@endsection