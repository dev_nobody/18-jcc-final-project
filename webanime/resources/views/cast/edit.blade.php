@extends('layout.master')
@section('crud')
Edit Cast
@endsection

@section('judul')
Halaman Edit Data Caster
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST" enctype="multipart/form-data">
    @csrf 
    @method('put')
    <div class="form-group">
    <label style="color:red;">Nama Caster</label>
    <input type="string" value="{{$cast->nama}}" class="form-control" name="nama">
    </div>

        @error('nama')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror

        <div class="form-group">
        <label style="color:red;">Umur</label>
        <textarea name="umur" class="form-control">{{$cast->umur}}</textarea>
        </div>

        @error('umur')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror
    
        <div class="form-group">
        <label style="color:red;">Bio</label>
        <textarea name="bio" value="{{$cast->bio}}" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/cast" class="btn btn-warning ml-5">Kembali</a>
        @include('sweetalert::alert')
@endsection
