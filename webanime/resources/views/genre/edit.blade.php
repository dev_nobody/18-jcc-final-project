@extends('layout.master')
@section('crud')
Genre
@endsection
@section('judul')
Halaman Edit Genre
@endsection

@section('content')
 
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="form-group">
      <label style="color:red;">Genre Film</label>
      <input type="text" name="nama" value="{{$genre->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/genre" class="btn btn-warning ml-5">Kembali</a>
</form>
@include('sweetalert::alert')
@endsection