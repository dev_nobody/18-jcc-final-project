@extends('layout.master')
@section('crud')
Genre
@endsection
@section('judul')
halaman list genre
@endsection


@section('content')

<a href="/genre/create" class="btn btn-info mb-3">Tambah Data</a>
<table class="table table-bordered table-dark">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Genre</th>
        <th scope="col">List Film</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <ul>
                        @foreach ($item->film as $value)
                            <li>{{$value->judul}}</li>
                        @endforeach
                     </ul>
                        
                </td>

                <td>
                    
                    <form action="/genre/{{$item->id}}" method="POST">
                        @method('delete')
                        @csrf
                          <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                          <a href="/genre/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
          <h1 style="color:red;"> Data Kosong</h1><br>
      @endforelse
    </tbody>
    
</table> 
@include('sweetalert::alert')

@endsection