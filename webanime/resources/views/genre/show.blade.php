@extends('layout.master')
@section('crud')
    Genre
@endsection

@section('judul')
    Halaman Detail Genre Film {{$genre->nama}}
@endsection

@section('content')

<h3 style="color:red;">{{$genre->nama}} <br><br></h3>
	<div class="row">
		@foreach ($genre->film as $item)
		<div class="col-4">
				<div class="card" style="width: 18rem;">
				<img src="{{asset('filmimage/'. $item->poster)}}" class="card-img-top" alt=" ">
				<div class="card-body">
					<h3>{{$item->judul}}</h3>
					<p class="card-text">{{($item->ringkasan)}}</p>    
			</div>
		</div>
		@endforeach
		<a href="/genre/" class="btn btn-info mt-3 ">Kembali</a>
	</div>

	@include('sweetalert::alert')
@endsection