<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('layout/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/elegant-icons.css')}}"type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/style.css')}}" type="text/css">
</head>

<body>
   

    <!-- Header Section Begin -->
    @include('partial.nav')
    <!-- Header End -->

    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="{{asset('layout/img/normal-breadcrumb.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>Login</h2>
                        <p>Welcome to the official Anime blog</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Normal Breadcrumb End -->

    <!-- Login Section Begin -->
    <section class="login spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login__form">
                        <h3>Login</h3>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="input__item">
                                <input type="text" name="email" placeholder="Email address">
                                <span class="icon_mail"></span>
                            </div>
                            <div class="input__item">
                                <input type="password" name="password" placeholder="Password">
                                <span class="icon_lock"></span>
                            </div>
                            <button type="submit" class="site-btn">Login Now</button>
                        </form>
                        <a href="#" class="forget_pass">Forgot Your Password?</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login__register">
                        <h3>Dont’t Have An Account?</h3>
                        <a href="/register" class="primary-btn">Register Now</a>
                    </div>
                </div>
            </div>
           
        </div>
    </section>
    <!-- Login Section End -->

    <!-- Footer Section Begin -->
    @include('partial.footer')
      <!-- Footer Section End -->

      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="{{asset('layout/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('layout/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('layout/js/player.js')}}"></script>
    <script src="{{asset('layout/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('layout/js/mixitup.min.js')}}"></script>
    <script src="{{asset('layout/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('layout/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('layout/js/main.js')}}"></script>


</body>

</html>