<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table ="film";

    protected $fillable =['judul','tahun','ringkasan','genre_id','poster'];

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function cast()
    {
        return $this->hasMany('App\cast');
    }

    public function kritik()
    {
        return $this->hasMany('App\Kritik');
    }

}