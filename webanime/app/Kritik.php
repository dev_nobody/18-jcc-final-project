<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table ="kritik";

    protected $fillable =['user_id','film_id','isi'];

    public function film()
    {
        return $this->belongsTo('App\Film','film_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
