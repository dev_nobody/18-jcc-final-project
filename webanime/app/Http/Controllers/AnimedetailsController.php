<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnimedetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        return view('layout.animedetails');
    }
}