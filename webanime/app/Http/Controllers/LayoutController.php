<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film;
use File;
use RealRashid\SweetAlert\Facades\Alert;
class LayoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $film = Film::all();
        return view('layout.layout', compact('film'));
    }

    
}