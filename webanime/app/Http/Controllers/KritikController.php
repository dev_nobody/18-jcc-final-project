<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Kritik;

class KritikController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'isi' => 'required'
           
        ]);

        $kritik = new Kritik;

        $kritik->isi = $request->isi;
        $kritik->user_id = Auth::id();
        $kritik->film_id = $request->film_id;

        $kritik->save();

        return redirect()->back();
    }

    public function destroy($id)
    {

        DB::table('kritik')->where('id', $id)->delete();
    }
           
    
    
}
