<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Peran;

class PeranController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'nama' => 'required'
           
        ]);

        $peran = new Peran;

        $peran->nama = $request->isi;
        $peran->cast_id = Auth::id();
        $peran->film_id = $request->film_id;

        $peran->save();

        return redirect()->back();
    }
    
}
