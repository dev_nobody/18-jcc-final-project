<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@index'); 
Route::post('/selesai','FormController@selesai');

Route::group(['middleware'=> ['auth']], function(){

// controller CRUD genre
Route::resource('genre','GenreController');
//CRUD Cast
Route::get('/cast/create', 'CastController@create');
// menyimpan data ke table cast
Route::post('/cast', 'CastController@store');
//tampil semua data
Route::get('/cast', 'CastController@index');
//detail data
Route::get('/cast/{cast_id}', 'CastController@show');
//mengarah ke form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data ke table cast
Route::put('/cast/{cast_id}', 'CastController@update');
//delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');

// Update Profile
Route::resource('profil','ProfileController')->only([
    'index', 'update'
]);
// Kritik
Route::resource('kritik','KritikController')->only([
    'index', 'store'
]);
Route::delete('/kritik/{kritik}','KritikController@destroy');


});


/*Route::get('/', function () {
    return view('layout.layout');
});*/

Route::get('/animedetails', 'AnimedetailsController@index');
Route::get('/', 'LayoutController@index');

// CRUD cast
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}','CastController@show');
Route::get('cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');

// CRUD film
Route::get('/film/create','FilmController@create');
Route::post('/film','FilmController@store');
Route::get('/film','FilmController@index');
Route::get('/film/{film_id}','FilmController@show');
Route::get('film/{film_id}/edit','FilmController@edit');
Route::put('/film/{film_id}','FilmController@update');
Route::delete('/film/{film_id}','FilmController@destroy');

Auth::routes();
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function (){
    \UniSharp\LaravelFilemanager\Lfm::routes();
});